string maxId = getMaxId();

                try
                {
                    int mId = Int32.Parse(maxId);
                    u.id = (mId++).ToString();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    u.id = "";
                }



private String getMaxId()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where exists(n.id) return max(n.id)",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            String maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList().FirstOrDefault();

            return maxId;
        }