
//korisnici aplikacije u bazi, dodaj jos 5, 6 korisnika 
create (user123:User {password:'123', name:'Danica', login:'danica'})
create (user1:User {password:'456', name:'Mihajlo', login:'mixa'})

//prijatelji
CREATE (user123)-[:IS_FRIEND]->(user1)
CREATE (user1)-[:IS_FRIEND]->(user123)

//stadioni, dodaj jos stadiona isto koliko i timova (jer su upareni)
CREATE (Stadion1:Stadion {naziv:'Marakana', klub:'Crvena Zvezda', kapacitet:50000})
CREATE (Stadion2:Stadion {naziv:'Allianz Arena', klub:'Bayern Munchen', kapacitet:500000})

//home vs away = utakmica, jos nekoliko utakmica
CREATE (Utakmica1:Utakmica {rezultat:'3-0'})
CREATE (Utakmica2:Utakmica {rezultat:'2-1'})

//korisnik ocenjuje utakmicu, koliko hoces ovih ocena, jedan korisnik moze da oceni vise utakmica, ali ne moze istu utakmicu dva puta
CREATE (user1)-[:RATING {stars:5, comment:'Odlicna utakmica!'}]->(Utakmica1)
CREATE (user1)-[:RATING {stars:2, comment:'Komentator uzasan'}]->(Utakmica2)

//vreme odigravanja utakmica
CREATE (Vreme1:Vreme {vreme_odrzavanja:'17:00', datum_odrzavanja:'01-MAY-2007'})
CREATE (Vreme2:Vreme {vreme_odrzavanja:'16:00', datum_odrzavanja:'05-MAY-2007'})

//relacije utakmica -> vreme igranja
CREATE (Utakmica1)-[:VREME_IGRANJA]->(Vreme1)
CREATE (Utakmica2)-[:VREME_IGRANJA]->(Vreme2)
CREATE (Utakmica2)-[:MESTO_IGRANJA]->(Stadion2)
CREATE (Utakmica1)-[:MESTO_IGRANJA]->(Stadion2)

//klubovi domaci
CREATE (Domaci1:Domacin {naziv_kluba:'Bayern Munchen', zemlja:'Nemacka'})

//klubovi gosti
CREATE (Gosti1:Gost {naziv_kluba:'Crvena Zvezda', zemlja:'Srbija'})

//veze utakmica sa domacim timom i sa timom gosti
//za utakmicu 1
CREATE (Utakmica1)-[:DOMACI_TIM]->(Domaci1)
CREATE (Utakmica1)-[:STRANI_TIM]->(Gosti1)

//za utakmicu 2
CREATE (Utakmica2)-[:DOMACI_TIM]->(Domaci1)
CREATE (Utakmica2)-[:STRANI_TIM]->(Gosti1)

//svetski kup, ovo je sve za jedan kup, jedan kup ima puno utakmica, dodaj sve za jedan, pa kad proverimo mozda ces za jos jedan
CREATE (Kup1:SvetskiKup {godina:'2007', drzava:'Nemacka'})

//kup 1 sadrzi utakmice 1 i 2 i sve ostale koje budes dodala
CREATE(Kup1)-[:SADRZI_UTAKMICU]->(Utakmica1)
CREATE(Kup1)-[:SADRZI_UTAKMICU]->(Utakmica2)
